package com.mx.devant.retailer.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan
@Configuration
@EnableJpaRepositories(basePackages = "com.mx.devant.retailer.api.entity")
@SpringBootApplication(scanBasePackages= "com.mx.devant.retailer.api")
public class DevantRetailerApp extends SpringBootServletInitializer{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(DevantRetailerApp.class);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(DevantRetailerApp.class, args);
	}

}
