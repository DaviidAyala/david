package com.mx.devant.retailer.api.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mx.devant.retailer.api.dao.RelTagLanguagesDAO;
import com.mx.devant.retailer.api.entity.RelTagLanguages;

@Repository
@Transactional(readOnly = true)
public class RelTagLanguagesDAOImpl implements RelTagLanguagesDAO {
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<RelTagLanguages> getcatLanguages(Integer tagId) {
		List<RelTagLanguages> list = null;
		try {
			TypedQuery<RelTagLanguages> query = em.createQuery("SELECT r FROM rel_tag_languages r WHERE r.tagId =:tagId", RelTagLanguages.class);
			query.setParameter("tagId", tagId);
			list = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
