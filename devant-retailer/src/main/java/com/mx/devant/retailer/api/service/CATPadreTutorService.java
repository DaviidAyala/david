package com.mx.devant.retailer.api.service;

import java.util.List;

import com.mx.devant.retailer.api.entity.PadreTutor;

public interface CATPadreTutorService {
	
	public List<PadreTutor> getPadreTutorByCURP(String curp);
}
