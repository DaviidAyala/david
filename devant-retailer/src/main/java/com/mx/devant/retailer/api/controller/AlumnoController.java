package com.mx.devant.retailer.api.controller;

import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.devant.retailer.api.entity.Alumno;
import com.mx.devant.retailer.api.response.Response;
import com.mx.devant.retailer.api.service.AlumnoService;

@CrossOrigin
@RestController
@RequestMapping(value = "/devant-config-api")
public class AlumnoController {
	
	@Autowired
	@Qualifier("alumnoServiceImpl")
	private AlumnoService alumnoService;
	
	@RequestMapping(value = "/info-alumno", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Response<List<Alumno>> getAllAlumnos() throws Exception{
		int code = HttpStatus.INTERNAL_SERVER_ERROR.value();
		String msg = "";
		boolean status = false;
		
		Response<List<Alumno>> response = new Response<List<Alumno>>();
		List<Alumno> data = alumnoService.getAllAlumno();
		
		if(ObjectUtils.equals(data, null)) {
			code = HttpStatus.NO_CONTENT.value();
			msg = "sin informacion";
			status = true;
		}else {
			code = HttpStatus.OK.value();
			msg = "Exito";
			status = true;
		}
		
		response.setCode(code);
		response.setMessage(msg);
		response.setStatus(status);
		response.setResponseBody(data);
		
		return response;
	}
}
