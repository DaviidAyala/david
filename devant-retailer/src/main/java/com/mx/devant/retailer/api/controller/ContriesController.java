package com.mx.devant.retailer.api.controller;

import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.devant.retailer.api.entity.CatCountries;
import com.mx.devant.retailer.api.response.Response;
import com.mx.devant.retailer.api.service.CatCountriesService;

@CrossOrigin
@RestController
@RequestMapping(value = "/devant-config-api")
public class ContriesController {
	
	@Autowired
	private CatCountriesService contriesService;
	
	@RequestMapping(value = "/info-contries-{country_id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody Response<List<CatCountries>> getContriesById(@PathVariable(name = "country_id", required = true)Integer country_id) throws Exception{
		int code = HttpStatus.INTERNAL_SERVER_ERROR.value();
		String msg = "";
		boolean status = false;
		
		List<CatCountries> country = contriesService.getCountryByID(country_id);
		Response<List<CatCountries>> response = new Response<List<CatCountries>>();
		
		if(ObjectUtils.equals(country, null)) {
			code = HttpStatus.NO_CONTENT.value();
			msg="sin informacion";
			status = true;
		}else {
			code = HttpStatus.OK.value();
			msg = "Exito";
			status = true;
		}
		
		response.setCode(code);
		response.setMessage(msg);
		response.setStatus(status);
		response.setResponseBody(country);
		
		return response;
	}
	
}
