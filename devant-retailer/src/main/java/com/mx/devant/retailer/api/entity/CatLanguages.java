package com.mx.devant.retailer.api.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "cat_languages")
@Table(name = "cat_languages")
public class CatLanguages implements Serializable{

	private static final long serialVersionUID = -583348937916749729L;
	
	@Id
	@Column(name = "language_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer languageId;
	
	@Column(name = "language_nm")
	private String languageNm;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "create_dt")
	private Timestamp createDt;
	
	@Column(name = "update_dt")
	private Timestamp updateDt;

	public Integer getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}

	public String getLanguageNm() {
		return languageNm;
	}

	public void setLanguageNm(String languageNm) {
		this.languageNm = languageNm;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}
}
