package com.mx.devant.retailer.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.devant.retailer.api.dao.CATPadreTutor;
import com.mx.devant.retailer.api.entity.PadreTutor;

@Service
public class CATPadreTutorServiceImpl implements CATPadreTutorService{
	
	@Autowired
	private CATPadreTutor catPadreTutorDAO;
	
	@Override
	public List<PadreTutor> getPadreTutorByCURP(String curp) {
		return catPadreTutorDAO.findByIdUsuario(curp);
	}

	

}
