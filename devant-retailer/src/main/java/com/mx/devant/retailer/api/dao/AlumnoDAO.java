package com.mx.devant.retailer.api.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mx.devant.retailer.api.entity.Alumno;

@Repository("alumnoDAO")
public interface AlumnoDAO extends CrudRepository<Alumno, Integer>{

}
