package com.mx.devant.retailer.api.response;

import java.io.Serializable;

public class Response<T> implements Serializable{

	private static final long serialVersionUID = 4417405564239282280L;
	private int code;
	private boolean status;
	private String message;
	private T responseBody;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getResponseBody() {
		return responseBody;
	}
	public void setResponseBody(T responseBody) {
		this.responseBody = responseBody;
	}
	
	public Response() {

	}
	
	public Response(int code, boolean status, String message, T responseBody) {
		this.code = code;
		this.status = status;
		this.message = message;
		this.responseBody = responseBody;
	}
	
	

}
