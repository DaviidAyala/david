package com.mx.devant.retailer.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.devant.retailer.api.dao.AlumnoDAO;
import com.mx.devant.retailer.api.entity.Alumno;
import com.mx.devant.retailer.api.exception.BusinessException;

@Service("alumnoServiceImpl")
public class AlumnoServiceImpl  implements AlumnoService{
	
	@Autowired
	private AlumnoDAO alumnoDAO;

	@Override
	public List<Alumno> getAllAlumno() throws BusinessException {
		List<Alumno> data = null;
		try {
			data = (List<Alumno>) alumnoDAO.findAll();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

}
