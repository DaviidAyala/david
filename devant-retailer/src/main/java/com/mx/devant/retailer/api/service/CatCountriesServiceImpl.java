package com.mx.devant.retailer.api.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.devant.retailer.api.dao.CatCountriesDAO;
import com.mx.devant.retailer.api.entity.CatCountries;
import com.mx.devant.retailer.api.exception.BusinessException;

@Service
public class CatCountriesServiceImpl implements CatCountriesService{
	
	private final Logger LOG = Logger.getLogger(CatCountriesServiceImpl.class);
	
	@Autowired
	private CatCountriesDAO catCountryDAO;
	
	@Override
	public List<CatCountries> getCountryByID(Integer countryId) throws BusinessException {
		
		List<CatCountries> list =null;
		try {
			list = catCountryDAO.findByCountryId(countryId);
		} catch (Exception e) {
			throw new BusinessException("Error de comunicacion");
		}
		
		return list;
	}

}
