package com.mx.devant.retailer.api.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity( name = "cat_countries")
@Table(name = "cat_countries")
public class CatCountries  implements Serializable{

	private static final long serialVersionUID = -388999453698204991L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer countryId;
	
	@Column(name="country_nm")
	private String countryNm;
	
	@Column(name="code_iso")
	private String codeIso;
	
	@Column(name="short_nm")
	private String shortNm;
	

	@Column(name="is_active")
	private int isActive;
	
	@Column(name="create_dt")
	private Timestamp createDt;
	
	@Column(name="update_dt")
	private Timestamp updateDt;

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryNm() {
		return countryNm;
	}

	public void setCountryNm(String countryNm) {
		this.countryNm = countryNm;
	}

	public String getCodeIso() {
		return codeIso;
	}

	public void setCodeIso(String codeIso) {
		this.codeIso = codeIso;
	}

	public String getShortNm() {
		return shortNm;
	}

	public void setShortNm(String shortNm) {
		this.shortNm = shortNm;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public Timestamp getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Timestamp createDt) {
		this.createDt = createDt;
	}
	
	

}
