package com.mx.devant.retailer.api.service;

import java.util.List;

import com.mx.devant.retailer.api.entity.RelTagLanguages;
import com.mx.devant.retailer.api.exception.BusinessException;

public interface RelTagLanguagesService {
	
	public List<RelTagLanguages> getCatLanguagesById(Integer tagId) throws BusinessException;
	
}
