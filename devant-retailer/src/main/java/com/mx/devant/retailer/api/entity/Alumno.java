package com.mx.devant.retailer.api.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "alumno")
@Table(name = "alumno" , schema = "escuela")
public class Alumno implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -839169012660466071L;
	
	@Id
	@Column(name = "numalumno")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer numAlumno;
	
	@Column(name = "curp")
	private String curp;
	
	@Column(name = "matricula")
	private String matricula;
	
	@Column(name = "escuela")
	private String escuela;
	
	@Column(name = "nivel")
	private String nivel;
	
	@Column(name = "grado")
	private String grado;
	
	@Column(name = "grupo")
	private String grupo;
	
	@Column(name = "idpadretutor")
	private String ipPadreTutor;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "apellido1")
	private String apellido1;
	
	@Column(name = "apellido2")
	private String apellido2;
	
	@Column(name = "fechanac")
	private Date fechaNac;
	
	@Column(name = "numhnos")
	private String numHnos;
	
	@Column(name = "poshnos")
	private String posHnos;
	
	@Column(name = "parentutor")
	private String parenTutor;
	
	@Column(name = "estatusalum")
	private String estatusalum;
	

	public Integer getNumAlumno() {
		return numAlumno;
	}

	public void setNumAlumno(Integer numAlumno) {
		this.numAlumno = numAlumno;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEscuela() {
		return escuela;
	}

	public void setEscuela(String escuela) {
		this.escuela = escuela;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getGrado() {
		return grado;
	}

	public void setGrado(String grado) {
		this.grado = grado;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getIpPadreTutor() {
		return ipPadreTutor;
	}

	public void setIpPadreTutor(String ipPadreTutor) {
		this.ipPadreTutor = ipPadreTutor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public Date getFechaNac() {
		return fechaNac;
	}

	public void setFechaNac(Date fechaNac) {
		this.fechaNac = fechaNac;
	}

	public String getNumHnos() {
		return numHnos;
	}

	public void setNumHnos(String numHnos) {
		this.numHnos = numHnos;
	}

	public String getPosHnos() {
		return posHnos;
	}

	public void setPosHnos(String posHnos) {
		this.posHnos = posHnos;
	}

	public String getParenTutor() {
		return parenTutor;
	}

	public void setParenTutor(String parenTutor) {
		this.parenTutor = parenTutor;
	}

	public String getEstatusalum() {
		return estatusalum;
	}

	public void setEstatusalum(String estatusalum) {
		this.estatusalum = estatusalum;
	}
}
