package com.mx.devant.retailer.api.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mx.devant.retailer.api.entity.PadreTutor;

@Repository("catPadreTutor")
public class CATPadreTutorDAOImpl {
	
	@Autowired
	@Qualifier("jdbcTemplateCore")
	private JdbcTemplate jdbcTemplate;
	
	
	public List<PadreTutor> findByIdUsuario(String idusuario) {
		
		StringBuilder stb = new StringBuilder();
		stb.append("select padretutor0_.idusuario as idusuari1_1_, ");
		stb.append("padretutor0_.cel_alt1 as cel_alt2_1_, padretutor0_.cel_alt2 as cel_alt3_1_, ");
		stb.append("padretutor0_.celular as celular4_1_, padretutor0_.email as email5_1_, ");
		stb.append("padretutor0_.escolaridad as escolari6_1_, padretutor0_.estadocivil as estadoci7_1_, ");
		stb.append("padretutor0_.nombre as nombre8_1_, padretutor0_.ocupacion as ocupacio9_1_, padretutor0_.tel_casa as tel_cas10_1_, ");
		stb.append("padretutor0_.tel_ofi as tel_ofi11_1_ from padretutor padretutor0_ where padretutor0_.idusuario = ?");
		
		List<PadreTutor> list = jdbcTemplate.query(stb.toString(), new Object[] { idusuario },
				new BeanPropertyRowMapper<PadreTutor>(PadreTutor.class));
		
		return list;
	}
	
//	public List<SelectedWeek> getSelectParameterToImpala(int serviceId, int groupId) {
//
//		String query = "SELECT period_id id, period_desc description FROM mars_datamart.lkp_period_group_rel"
//				+ " WHERE group_id = " + groupId + " and country_service_id = " + serviceId + " ORDER BY period_id";
//
//		List<SelectedWeek> list = (List<SelectedWeek>) jdbcTemplateImpala.query(query, new RowMapper<SelectedWeek>() {
//			public SelectedWeek mapRow(ResultSet rs, int rowNum) throws SQLException {
//				SelectedWeek item = new SelectedWeek();
//				item.setId(rs.getInt("id"));
//				item.setDescription(rs.getString("description"));
//				return item;
//			}
//		});
//
//		return list;
//	}

}
