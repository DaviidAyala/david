package com.mx.devant.retailer.api.service;

import java.util.List;

import com.mx.devant.retailer.api.entity.Employee;
import com.mx.devant.retailer.api.exception.BusinessException;

public interface EmployeeService {
	
	public List<Employee> getAllEmployee() throws BusinessException;
	
	public Employee getEmployeeByID(int id) throws BusinessException;
}
