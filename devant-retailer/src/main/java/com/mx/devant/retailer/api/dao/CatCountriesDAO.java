package com.mx.devant.retailer.api.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mx.devant.retailer.api.entity.CatCountries;

@Repository
public interface CatCountriesDAO extends CrudRepository<CatCountries, Integer> {
	
	public List<CatCountries> findByCountryId(Integer countryId);

}
