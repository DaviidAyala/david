package com.mx.devant.retailer.api.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mx.devant.retailer.api.dao.EmployeeTypedDAO;
import com.mx.devant.retailer.api.entity.Employee;

@Repository
@Transactional(readOnly = true)
public class EmployeeDAOImpl implements EmployeeTypedDAO{
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public Employee findById(int id) {
		TypedQuery<Employee> query = em.createQuery("SELECT e FROM employee e WHERE e.id =:id ", Employee.class);
		query.setParameter("id", id);
		List<Employee> list = query.getResultList();
		return ( list == null || list.size() == 0 ? null : list.get(0) );
	}
	
	
	
}
