package com.mx.devant.retailer.api.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
@PropertySource(value = { "classpath:application.properties" })
public class DataSourceConfig {
	
	@Bean(name = "dataSourceCore")
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource dataSourceCore() {
		return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "jdbcTemplateCore")
	@Autowired
	public JdbcTemplate jdbcTemplateCore(@Qualifier("dataSourceCore") DataSource dataSourceCore) {
		return new JdbcTemplate(dataSourceCore);
	}
	
	
	
	

	
}
