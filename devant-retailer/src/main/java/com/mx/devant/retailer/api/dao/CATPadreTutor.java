package com.mx.devant.retailer.api.dao;

import java.util.List;

import com.mx.devant.retailer.api.entity.PadreTutor;

public interface CATPadreTutor{
	
	public List<PadreTutor> findByIdUsuario(String idusuario);
	
}
