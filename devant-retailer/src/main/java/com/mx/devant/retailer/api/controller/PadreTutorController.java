package com.mx.devant.retailer.api.controller;

import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.devant.retailer.api.entity.PadreTutor;
import com.mx.devant.retailer.api.response.Response;
import com.mx.devant.retailer.api.service.CATPadreTutorService;

@CrossOrigin
@RestController
@RequestMapping(value = "/devant-config-api")
public class PadreTutorController {
	
	@Autowired
	private CATPadreTutorService catPadreTutorService;
	
	@RequestMapping(value = "/info-padre-tutro/{curp}")
	public Response<List<PadreTutor>> getPadreTutorByCURP(@PathVariable(name = "curp", required = true)String curp) throws Exception{
		int code = HttpStatus.INTERNAL_SERVER_ERROR.value();
		String msg = "";
		boolean status = false;
		
		Response<List<PadreTutor>> response = new Response<List<PadreTutor>>();
		List<PadreTutor> data = catPadreTutorService.getPadreTutorByCURP(curp);
		
		if(ObjectUtils.equals(data, null)) {
			code = HttpStatus.NO_CONTENT.value();
			msg = "sin informacion";
			status = true;
		}else {
			code = HttpStatus.OK.value();
			msg = "exito";
			status = true;
		}
		
		response.setCode(code);
		response.setMessage(msg);
		response.setStatus(status);
		response.setResponseBody(data);
		
		return response;
		
	}
}
