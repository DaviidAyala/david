package com.mx.devant.retailer.api.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "padretutor")
@Table(name = "padretutor", schema = "escuela")
public class PadreTutor implements Serializable{

	private static final long serialVersionUID = 723998825979360536L;
	
	@Id
	@Column(name = "idusuario",unique=true,nullable=false)
	private String idUsuario;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "escolaridad")
	private String escolaridad;
	
	@Column(name = "ocupacion")
	private String ocupacion;
	
	@Column(name = "estadocivil")
	private String estadoCivil;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "celular")
	private String celular;
	
	@Column(name = "cel_alt1")
	private String celAlt1;
	
	@Column(name = "cel_alt2")
	private String celAlt2;
	
	@Column(name = "tel_casa")
	private String telCasa;
	
	@Column(name = "tel_ofi")
	private String telOfi;
	
	@OneToMany(mappedBy = "numAlumno", cascade = CascadeType.ALL)
	private List<Alumno> alumnosList;
}
