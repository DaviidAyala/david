package com.mx.devant.retailer.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.devant.retailer.api.dao.EmployeeDAO;
import com.mx.devant.retailer.api.dao.EmployeeTypedDAO;
import com.mx.devant.retailer.api.entity.Employee;
import com.mx.devant.retailer.api.exception.BusinessException;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private EmployeeTypedDAO empTypedDAO;
	
	@Override
	public List<Employee> getAllEmployee() throws BusinessException {
		List<Employee> list = null;
		try {
			list = (List<Employee>) employeeDAO.findAll();
		} catch (Exception e) {
			throw new BusinessException("Error de comunicación", e);
		}
		return list;
	}

	@Override
	public Employee getEmployeeByID(int id) throws BusinessException{
		return empTypedDAO.findById(id);
	}

}
