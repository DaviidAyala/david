package com.mx.devant.retailer.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.devant.retailer.api.entity.Employee;
import com.mx.devant.retailer.api.response.Response;
import com.mx.devant.retailer.api.service.EmployeeService;

@CrossOrigin
@RestController
@RequestMapping(value = "/devant-config-api")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	
	@RequestMapping(value = "/info-deployee", method = RequestMethod.GET)
	public @ResponseBody Response<List<Employee>> getAllEmployee() throws Exception{
		String msg = "";
		boolean status = false;
		int code = HttpStatus.INTERNAL_SERVER_ERROR.value();
		
		Response<List<Employee>> response = new Response<List<Employee>>();
		List<Employee> list = null;
		
		try {
			list = employeeService.getAllEmployee();
			if (!(list != null && list.size() > 0)) {
				code = HttpStatus.NO_CONTENT.value();
				msg = "sin informacion";
				status = true;
			}else {
				code = HttpStatus.OK.value();
				msg = "Exito";
				status = true;
			}
		} catch (Exception bus) {
			code = HttpStatus.CONFLICT.value();
			msg = bus.getMessage();
			status = false;
		}
		
		response.setCode(code);
		response.setMessage(msg);
		response.setStatus(status);
		response.setResponseBody(list);
		
		return response;
		
	}
	
	@RequestMapping(value = "/info-deployee-{id}")
	public @ResponseBody Response<Employee> getEmployeeByID(@PathVariable(name = "id", required = true) int id) throws Exception{
		Employee list = employeeService.getEmployeeByID(id);
		Response<Employee> response = new Response<Employee>();
		
		if (!(list != null)) {
			response.setCode(HttpStatus.NO_CONTENT.value());
			response.setMessage("sin informacion");
			response.setStatus(false);
		}else {
			
			response.setCode(HttpStatus.OK.value());
			response.setMessage("Exito");
			response.setResponseBody(list);
			response.setStatus(true);
			
		}
		
		return response;
	}
}
