package com.mx.devant.retailer.api.service;

import java.util.List;

import com.mx.devant.retailer.api.entity.Alumno;
import com.mx.devant.retailer.api.exception.BusinessException;

public interface AlumnoService {
	
	public List<Alumno> getAllAlumno()throws BusinessException;
}
