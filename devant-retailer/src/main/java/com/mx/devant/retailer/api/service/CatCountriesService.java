package com.mx.devant.retailer.api.service;

import java.util.List;

import com.mx.devant.retailer.api.entity.CatCountries;
import com.mx.devant.retailer.api.exception.BusinessException;

public interface CatCountriesService {
	
	public List<CatCountries> getCountryByID(Integer country_id)throws BusinessException;
}
