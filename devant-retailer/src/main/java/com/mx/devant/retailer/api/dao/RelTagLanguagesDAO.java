package com.mx.devant.retailer.api.dao;

import java.util.List;

import com.mx.devant.retailer.api.entity.RelTagLanguages;

public interface RelTagLanguagesDAO {

	public List<RelTagLanguages> getcatLanguages(Integer tagId);
	
}
