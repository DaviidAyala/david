package com.mx.devant.retailer.api.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "rel_tag_languages")
@Table(name = "rel_tag_languages")
public class RelTagLanguages implements Serializable{

	private static final long serialVersionUID = -3384964643765540276L;
	
	@Id
	@Column(name = "tag_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer tagId;
	
	@Column(name = "language_id")
	private Integer languageId;
	
	@Column(name = "tooltip")
	private String tooltip;
	
	@Column(name = "text_value")
	private String textValue;
	
	@OneToMany(mappedBy = "languageId")
	private List<CatLanguages> languages;

	public Integer getTagId() {
		return tagId;
	}

	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}

	public Integer getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}

	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public String getTextValue() {
		return textValue;
	}

	public void setTextValue(String textValue) {
		this.textValue = textValue;
	}
	
}
