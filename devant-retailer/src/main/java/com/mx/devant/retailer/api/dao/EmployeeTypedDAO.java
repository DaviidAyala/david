package com.mx.devant.retailer.api.dao;

import com.mx.devant.retailer.api.entity.Employee;

public interface EmployeeTypedDAO {
	
	public Employee findById(int id);
}
