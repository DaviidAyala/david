package com.mx.devant.retailer.api.controller;

import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.devant.retailer.api.entity.RelTagLanguages;
import com.mx.devant.retailer.api.response.Response;
import com.mx.devant.retailer.api.service.RelTagLanguagesService;

@CrossOrigin
@RestController
@RequestMapping(value = "/devant-config-api")
public class RelTagLanguagesController {

	@Autowired
	private RelTagLanguagesService relTagLanguagesService;

	@RequestMapping(value = "/info-tag-languages-{relTagId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody Response<List<RelTagLanguages>> getTagLenaguagesById(@PathVariable(name = "relTagId", required = true)Integer relTagId)throws Exception {
		int code = HttpStatus.INTERNAL_SERVER_ERROR.value();
		String msg = "";
		boolean status = false;
		
		List<RelTagLanguages> list = relTagLanguagesService.getCatLanguagesById(relTagId);
		Response<List<RelTagLanguages>> response = new Response<List<RelTagLanguages>>();

		if (ObjectUtils.equals(list, null)) {
			code = HttpStatus.NO_CONTENT.value();
			status = true;
			msg = "sin informacion";
		} else {
			code = HttpStatus.OK.value();
			status = true;
			msg = "Exito";
		}

		response.setCode(code);
		response.setMessage(msg);
		response.setStatus(status);
		response.setResponseBody(list);

		return response;
	}
}
