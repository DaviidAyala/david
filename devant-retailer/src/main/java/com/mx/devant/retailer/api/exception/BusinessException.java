package com.mx.devant.retailer.api.exception;

public class BusinessException extends Exception{

	private static final long serialVersionUID = -5958455875097664719L;

	public BusinessException() {	
	}

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public BusinessException(String message) {
		super(message);
	}

	public BusinessException(Throwable cause) {
		super(cause);
	}
	
	
	
}
