package com.mx.devant.retailer.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.devant.retailer.api.dao.RelTagLanguagesDAO;
import com.mx.devant.retailer.api.entity.RelTagLanguages;
import com.mx.devant.retailer.api.exception.BusinessException;

@Service
public class RelTagLanguagesServiceImpl implements RelTagLanguagesService{
	
	@Autowired
	private RelTagLanguagesDAO relTagDAO;
	
	@Override
	public List<RelTagLanguages> getCatLanguagesById(Integer tagId) throws BusinessException{
		List<RelTagLanguages> list = null;
		try {
			list = relTagDAO.getcatLanguages(tagId);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException("Error de comunicacion");
		}
		return list;
	}

}
