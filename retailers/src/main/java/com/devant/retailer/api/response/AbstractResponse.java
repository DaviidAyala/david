package com.devant.retailer.api.response;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

public class AbstractResponse<T> implements Serializable{

	private static final long serialVersionUID = 7805943485793351451L;
	private HttpStatus status;
	private T responseBody;

	public AbstractResponse() {
	}

	public AbstractResponse(HttpStatus status, T responseBody) {
		this.status = status;
		this.responseBody = responseBody;
	}

	public int getStatus() {
		return status.value();
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public T getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(T responseBody) {
		this.responseBody = responseBody;
	}
}
