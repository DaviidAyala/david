package com.devant.retailer.api.response;

public interface MessageService {
	public String getMessage(String id);
}
