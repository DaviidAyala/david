package com.devant.retailer.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.devant.retailer.api.entity.Employee;
import com.devant.retailer.api.response.Response;
import com.devant.retailer.api.service.EmployeeService;

@CrossOrigin
@RestController
@RequestMapping(value = "/devant-config-api")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/devant-employee", method = { RequestMethod.GET })
	public @ResponseBody ResponseEntity<Response<List<Employee>>> getAllEmployee(){
		List<Employee> list = employeeService.getAllEmployee();
		Response<List<Employee>> response;
		String msg = "";
		
		response = new Response<List<Employee>>(list, msg);
		return new ResponseEntity<Response<List<Employee>>>(response, HttpStatus.valueOf(response.getStatus()));
	}
}
