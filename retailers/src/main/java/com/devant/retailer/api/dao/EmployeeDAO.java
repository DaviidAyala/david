package com.devant.retailer.api.dao;

import java.util.List;

import com.devant.retailer.api.entity.Employee;

public interface EmployeeDAO {
	
	public List<Employee> findAll();
}
