package com.devant.retailer.api.response;

public abstract class AbstractErrorInfo {

	private String errorId;
	private String description;
	private ErrorSeverity severity;

	public AbstractErrorInfo(String errorId, String description, ErrorSeverity severity) {
		this.errorId = errorId;
		this.description = description;
		this.severity = severity;
	}

	public String getErrorId() {
		return errorId;
	}

	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ErrorSeverity getSeverity() {
		return severity;
	}

	public void setSeverity(ErrorSeverity severity) {
		this.severity = severity;
	}

}
