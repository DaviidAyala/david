package com.devant.retailer.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devant.retailer.api.dao.EmployeeDAO;
import com.devant.retailer.api.entity.Employee;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	
	public List<Employee> getAllEmployee(){
		return employeeDAO.findAll();
	}
}
