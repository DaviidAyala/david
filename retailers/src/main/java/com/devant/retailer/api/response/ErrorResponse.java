package com.devant.retailer.api.response;

import java.util.List;

import org.springframework.http.HttpStatus;

public class ErrorResponse extends AbstractResponse<InnerErrorResponse> {

	private static final long serialVersionUID = -6398727373575579172L;

	public ErrorResponse() {
		super();
	}

	public ErrorResponse(HttpStatus status) {
		super(status, new InnerErrorResponse(status.name()));
	}

	public ErrorResponse(HttpStatus status, String exception) {
		super(status, new InnerErrorResponse(status.name(), exception));
	}

	public ErrorResponse(HttpStatus status, String exception, List<String> errorMessages) {
		super(status, new InnerErrorResponse(status.name(), exception, errorMessages));
	}

	public ErrorResponse(HttpStatus status, String exception, List<String> errorMessages, List<String> messages) {
		super(status, new InnerErrorResponse(status.name(), exception, errorMessages, messages));
	}

}
