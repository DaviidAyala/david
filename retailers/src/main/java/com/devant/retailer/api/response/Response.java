package com.devant.retailer.api.response;

import org.springframework.http.HttpStatus;

public class Response<T> extends AbstractResponse<T> {

	private static final long serialVersionUID = -3457271437107479504L;
	private String message;
	
	public Response() {
	
	}
	
	public Response(T responseBody) {
		super(HttpStatus.OK, responseBody);
	}

	public Response(T responseBody, String message) {
		super(HttpStatus.OK, responseBody);
		this.message = message;
	}

	public Response(HttpStatus status, T responseBody, String message) {
		super(status, responseBody);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
