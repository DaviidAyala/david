package com.devant.retailer.api.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InnerErrorResponse implements Serializable {

	private static final long serialVersionUID = 4440012707337595629L;

	private String error;
	private String exception;
	private List<String> messages;
	private List<String> errorMessages;

	public InnerErrorResponse() {
		this.messages = new ArrayList<String>();
		this.errorMessages = new ArrayList<String>();
	}

	public InnerErrorResponse(String error) {
		this.error = error;
		this.messages = new ArrayList<String>();
		this.errorMessages = new ArrayList<String>();
	}

	public InnerErrorResponse(String error, String exception) {
		this.error = error;
		this.exception = exception;
		this.messages = new ArrayList<String>();
		this.errorMessages = new ArrayList<String>();
	}

	public InnerErrorResponse(String error, String exception, List<String> errorMessages) {
		this.error = error;
		this.exception = exception;
		this.errorMessages = errorMessages;
		this.messages = new ArrayList<String>();
	}

	public InnerErrorResponse(String error, String exception, List<String> errorMessages, List<String> messages) {
		this.error = error;
		this.exception = exception;
		this.errorMessages = errorMessages;
		this.messages = messages;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return this.error;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getException() {
		return this.exception;
	}

	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public void addErrorMessage(String message) {
		this.errorMessages.add(message);
	}

	public void addAllErrorMessages(List<String> messages) {
		this.errorMessages.addAll(messages);
	}

	public List<String> getMessages() {
		return this.messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public void addMessage(String message) {
		this.messages.add(message);
	}

	public void addAllMessages(List<String> messages) {
		this.messages.addAll(messages);
	}

}