package com.devant.retailer.api.response;

public enum ErrorSeverity {
	UNKNOWN, FATAL, ERROR, WARNING
}
