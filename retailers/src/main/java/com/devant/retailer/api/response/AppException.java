package com.devant.retailer.api.response;

import org.springframework.http.HttpStatus;

public class AppException extends RuntimeException {

	private static final long serialVersionUID = -8658840049487176966L;

	private AbstractErrorInfo errorInfo;
	private HttpStatus status;

	protected AppException() {
	}

	public AppException(AbstractErrorInfo errorInfo) {
		super(errorInfo.getDescription());
		this.errorInfo = errorInfo;
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
	}

	public AppException(AbstractErrorInfo errorInfo, HttpStatus status) {
		super(errorInfo.getDescription());
		this.errorInfo = errorInfo;
		this.status = status;
	}

	public AppException(AbstractErrorInfo errorInfo, String message, Throwable cause) {
		super(message, cause);
		this.errorInfo = errorInfo;
	}

	public AppException(AbstractErrorInfo errorInfo, String message, HttpStatus status) {
		super(message);
		this.errorInfo = errorInfo;
		this.status = status;
	}

	public AppException(AbstractErrorInfo errorInfo, Throwable cause) {
		super(cause);
		this.errorInfo = errorInfo;
	}

	public AbstractErrorInfo getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(AbstractErrorInfo errorInfo) {
		this.errorInfo = errorInfo;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
}
