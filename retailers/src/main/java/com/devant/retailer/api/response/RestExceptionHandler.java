package com.devant.retailer.api.response;

import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@ControllerAdvice
@RestController
public class RestExceptionHandler {

	final static Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

	@Autowired
	private MessageService messageSource;

	@ExceptionHandler(Exception.class)
	public @ResponseBody ResponseEntity<ErrorResponse> handleException(Exception ex) {
		String msg = messageSource.getMessage("api.commons.exceptions.1005");
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		ErrorResponse response = new ErrorResponse(status, ex.getClass().getName());
		response.getResponseBody().addMessage(msg);
		response.getResponseBody().addErrorMessage(ex.getMessage());

		return errorResponse(ex, status, response);
	}

	@ExceptionHandler({ IllegalArgumentException.class, HttpMessageNotReadableException.class,
			InvocationTargetException.class, ClassCastException.class, ConversionFailedException.class })
	public @ResponseBody ResponseEntity<ErrorResponse> handleIlegalArgumentException(Exception ex) {
		String msg = messageSource.getMessage("api.commons.exceptions.1006");
		HttpStatus status = HttpStatus.BAD_REQUEST;
		ErrorResponse response = new ErrorResponse(status, ex.getClass().getName());
		response.getResponseBody().addMessage(msg);
		response.getResponseBody().addErrorMessage(ex.getMessage());
		return errorResponse(ex, status, response);
	}

	@ExceptionHandler(AppException.class)
	public @ResponseBody ResponseEntity<ErrorResponse> handleAppException(AppException ex) {
		HttpStatus status = ex.getStatus();
		ErrorResponse response = new ErrorResponse(status, ex.getClass().getName());
		response.getResponseBody().setError(ex.getErrorInfo().getErrorId());
		response.getResponseBody().addMessage(ex.getMessage());
		if (ex.getCause() != null)
			response.getResponseBody().addErrorMessage(ex.getCause().getMessage());

		return errorResponse(ex, status, response);
	}

	protected ResponseEntity<ErrorResponse> errorResponse(Throwable throwable, HttpStatus status,
			ErrorResponse errorBody) {
		if (throwable != null) {
			logger.error("Error caught: " + throwable.getMessage(), throwable);
			return response(errorBody, status);
		} else {
			logger.error("Unknown error caught in RestController, {}", status);
			return response(errorBody, status);
		}
	}

	protected <T> ResponseEntity<T> response(T body, HttpStatus status) {
		return new ResponseEntity<T>(body, status);
	}

}