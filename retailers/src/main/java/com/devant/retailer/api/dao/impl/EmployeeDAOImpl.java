package com.devant.retailer.api.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.devant.retailer.api.dao.EmployeeDAO;
import com.devant.retailer.api.entity.Employee;

@Repository
@Transactional
public class EmployeeDAOImpl implements EmployeeDAO{

	@PersistenceContext
	EntityManager em;
	
	@Override
	public List<Employee> findAll() {
		TypedQuery<Employee> query = em.createQuery("SELECT e FROM employee e order by id", Employee.class);
		return query.getResultList();
	}

}
