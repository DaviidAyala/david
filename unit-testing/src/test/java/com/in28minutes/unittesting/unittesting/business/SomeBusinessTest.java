package com.in28minutes.unittesting.unittesting.business;

import static org.junit.Assert.*;

import org.junit.Test;

import com.in28minutes.unittesting.unittesting.bisiness.SomeBusinessImpl;
import com.in28minutes.unittesting.unittesting.data.SomeDataService;

class SomeDataServiceStub implements SomeDataService{
	@Override
	public int[] retrieveAllData() {
		return new int[] {1,2,3};
	}	
}

class SomeDataServiceEmptyStub implements SomeDataService{
	@Override
	public int[] retrieveAllData() {
		return new int[] {};
	}	
}

class SomeDataServiceOneElementStub implements SomeDataService{
	@Override
	public int[] retrieveAllData() {
		return new int[] { 5 };
	}	
}

public class SomeBusinessTest {

	@Test
	public void calculateSumUsingDataService() {
		SomeBusinessImpl business = new SomeBusinessImpl();
		business.setSomeDataService(new SomeDataServiceStub());
		int actualResult = business.calculateSumUsingDataService();
		int expectedResult = 6;
		assertEquals(expectedResult, actualResult);
	}
	
	@Test
	public void calculateSumUsingDataService_empty() {
		SomeBusinessImpl business = new SomeBusinessImpl();
		business.setSomeDataService(new SomeDataServiceEmptyStub());
		int actualResult = business.calculateSumUsingDataService(); //new int[] {}
		int expectedResult = 0;
		assertEquals(expectedResult, actualResult);
	}
	
	@Test
	public void calculateSumUsingDataService_oneValue() {
		SomeBusinessImpl business = new SomeBusinessImpl();
		business.setSomeDataService(new SomeDataServiceOneElementStub());
		int actualResult = business.calculateSumUsingDataService(); //new int[] { 5 }
		int exceptedResult = 5;
		assertEquals(exceptedResult, actualResult);
	}
}
