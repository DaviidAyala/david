package com.luv2code.springboot.demo.mycoolapp.rest;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luv2code.springboot.demo.mycoolapp.dto.CheckInOutDTO;
import com.luv2code.springboot.demo.mycoolapp.dto.UserDevant;
import com.luv2code.springboot.demo.mycoolapp.service.Test2;

@RestController
@RequestMapping("/contro-asistencia-devant")
public class FunRestController {
	
	@Autowired
	private Test2 te;
	
	@GetMapping("/user-devant")
	public List<UserDevant> getAllUser() throws Exception{
		List<UserDevant> users= te.getAllUserDevat();
		return users;
	}
	
	@GetMapping("/user-devant/{userDevantId}")
	public List<UserDevant> getUserDevantId(@PathVariable int userDevantId) throws Exception{
		List<UserDevant> users = te.getUserDevant(userDevantId);
		return users;
	}
	
	@GetMapping("/user-devant/check-in-out/{userDevantId}")
	public List<CheckInOutDTO> getCheckInOut(@PathVariable int userDevantId) throws Exception{
		return te.getCheckInOut(userDevantId);
	}
	
	
}
