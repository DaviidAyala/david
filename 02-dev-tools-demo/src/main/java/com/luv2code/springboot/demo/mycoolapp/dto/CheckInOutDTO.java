package com.luv2code.springboot.demo.mycoolapp.dto;

import java.io.Serializable;
import java.util.Date;


public class CheckInOutDTO implements Serializable{

	private static final long serialVersionUID = 4116470496775982859L;
	
	private Integer userID;
	private String date;
	
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	

}
