package com.luv2code.springboot.demo.mycoolapp.config;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ControllerDb {
	
	@Autowired
	private ConnectionDB con;
	
	public ControllerDb() {
		this.con = new ConnectionDB();
	}
	
	public void crearConexion() {
		this.con.establecerConexion();
	}
	
	public ResultSet mandarSql(String sql) throws SQLException {
		ResultSet aux_result = this.con.ejecutarSentencia(sql);
		return aux_result;
	}
}
