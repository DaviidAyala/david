package com.luv2code.springboot.demo.mycoolapp.dto;

import java.io.Serializable;
import java.util.List;

public class UserDevant implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8929139644582186850L;
	
	private Integer userID;
	private Integer badgerNumber;
	private String name;
	private String title;
	private List<CheckInOutDTO> checkInOut;
	
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public Integer getBadgerNumber() {
		return badgerNumber;
	}
	public void setBadgerNumber(Integer badgerNumber) {
		this.badgerNumber = badgerNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<CheckInOutDTO> getCheckInOut() {
		return checkInOut;
	}
	public void setCheckInOut(List<CheckInOutDTO> checkInOut) {
		this.checkInOut = checkInOut;
	}
	
}
