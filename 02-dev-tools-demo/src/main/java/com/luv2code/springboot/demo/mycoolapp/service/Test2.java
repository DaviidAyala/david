package com.luv2code.springboot.demo.mycoolapp.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luv2code.springboot.demo.mycoolapp.config.ControllerDb;
import com.luv2code.springboot.demo.mycoolapp.dto.CheckInOutDTO;
import com.luv2code.springboot.demo.mycoolapp.dto.UserDevant;

@Service
public class Test2 {

	@Autowired
	public ControllerDb controllerDB;

	public List<UserDevant> getAllUserDevat() throws SQLException, NumberFormatException, ParseException {
		String sql = "SELECT * FROM USERINFO";

		controllerDB = new ControllerDb();
		controllerDB.crearConexion();
		ResultSet rs;

		rs = controllerDB.mandarSql(sql);

		List<UserDevant> listUserDev = new ArrayList<UserDevant>();

		while (rs.next()) {
			UserDevant user = new UserDevant();
			user.setUserID(Integer.parseInt(rs.getString(1)));
			user.setBadgerNumber(Integer.parseInt(rs.getString(2)));
			user.setName(rs.getString(4));
			user.setTitle(rs.getString(6));
			user.setCheckInOut(this.getCheckInOut(Integer.parseInt(rs.getString(1))));
			listUserDev.add(user);
		}

		return listUserDev;
	}

	public List<UserDevant> getUserDevant(int id) throws SQLException, NumberFormatException, ParseException {
		controllerDB = new ControllerDb();
		controllerDB.crearConexion();
		ResultSet rs;

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM USERINFO WHERE USERID =");
		sb.append(id);
		rs = controllerDB.mandarSql(sb.toString());
		List<UserDevant> listUserDev = new ArrayList<UserDevant>();

		while (rs.next()) {
			UserDevant user = new UserDevant();
			user.setUserID(Integer.parseInt(rs.getString(1)));
			user.setBadgerNumber(Integer.parseInt(rs.getString(2)));
			user.setName(rs.getString(4));
			user.setTitle(rs.getString(6));
			user.setCheckInOut(this.getCheckInOut(Integer.parseInt(rs.getString(1))));
			listUserDev.add(user);
		}

		return listUserDev;
	}
	
	public List<CheckInOutDTO> getCheckInOut(int userDevantId) throws SQLException, ParseException{
		controllerDB = new ControllerDb();
		controllerDB.crearConexion();
		ResultSet rs;
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM CHECKINOUT WHERE USERID =");
		sb.append(userDevantId);
		sb.append(" ORDER BY CHECKTIME DESC");
		rs = controllerDB.mandarSql(sb.toString());
		List<CheckInOutDTO> listck = new ArrayList<CheckInOutDTO>();
		while (rs.next()) {
			CheckInOutDTO ck = new CheckInOutDTO();
			ck.setUserID(Integer.parseInt(rs.getString(1)));
	        SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        Date date = displayFormat.parse(rs.getString(2));
	        
	        SimpleDateFormat disprmat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	        String dt = disprmat.format(date);
	        
	        ck.setDate(dt);
	        listck.add(ck);
		}
		
		String[] arr = {"01-03-2019 08:32:43", "28-03-2019 09:13:23"};
		this.validarRetardos(listck, arr);
		return listck;
	}
	
	public List<CheckInOutDTO> validarRetardos(List<CheckInOutDTO> data, String[] fechas) throws ParseException {
		
		 for(String u : fechas) {
			  
			  SimpleDateFormat disprmat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			  Date date = disprmat.parse(u);
		      
		      SimpleDateFormat formatDias = new SimpleDateFormat("dd");
			  String getDias = formatDias.format(date);

			  SimpleDateFormat formatMes = new SimpleDateFormat("MM");
			  String getMes = formatMes.format(date);
			  
			  SimpleDateFormat formatAnio = new SimpleDateFormat("yyyy");
			  String getAnio = formatAnio.format(date);
		      
			  Calendar c = Calendar.getInstance();
			  int mes = c.get(Calendar.MONTH);
			  int dia = c.get(Calendar.DAY_OF_MONTH);
			  
			  if(Integer.parseInt(getDias) <= dia 
					  && Integer.parseInt(getMes) == mes+1 ) {
				  
				  System.out.println(u);
			  }   
		  }
		
		
//		for (CheckInOutDTO u : data) {
//
//			SimpleDateFormat formatMinutes = new SimpleDateFormat("mm");
//			String getMinutes = formatMinutes.format(u.getDate());
//
//			SimpleDateFormat formatHours = new SimpleDateFormat("HH");
//			String getHours = formatHours.format(u.getDate());
//
//			System.out.println("hora: " + getHours + "Minutes: " + getMinutes);
//			if (Integer.parseInt(getHours) <= 9 && Integer.parseInt(getHours) < 10) {
//				if (Integer.parseInt(getMinutes) < 15
//						|| (Integer.parseInt(getHours) <= 8 && Integer.parseInt(getMinutes) <= 60)) {
//					System.out.println("Entro a tiempo :p");
//				} else {
//					System.out.println("Llego tarde :(");
//				}
//			} else {
//				System.out.println("Llego tarde :(");
//			}
//
//		}

		return null;
	}

}
