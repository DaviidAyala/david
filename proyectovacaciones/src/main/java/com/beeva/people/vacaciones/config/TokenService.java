package com.beeva.people.vacaciones.config;

import io.jsonwebtoken.*;
import org.springframework.security
        .authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

import static java.util.Collections.emptyList;

/**
 * This service class is which is responsible for all about JWT Token.
 * @author juan
 */
public class TokenService {

    static final long EXPIRATIONTIME = 864_000_000; // 10 days
    static final String SECRET = "ThisIsASecret";
    static final String TOKEN_PREFIX = "Bearer ";
    static final String HEADER_STRING = "Authorization";

    /**
     * Method which creates jwt token.
     * @param res response that will be sent back to the client but with JWT token in header
     * @param username username from basic authentication
     */
    static void addAuthentication(HttpServletResponse res, String username) {
        String JWT = Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
    }


    /**
     * This method verifies JWT Tokens veracity
     * and return an instance of Authentication if all in token is okay,
     * if not, it return null and unchains 403 HTTP error
     *
     * @param request request from client to extract the jwt token and check it
     * @return instance of Authentication
     * @throws MalformedJwtException
     */
    static Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);

        if (token != null && token.startsWith(TOKEN_PREFIX)) {
            try {
                String user = Jwts.parser()
                        .setSigningKey(SECRET)
                        .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                        .getBody()
                        .getSubject();

                return new UsernamePasswordAuthenticationToken(user, null, emptyList());
            } catch (ExpiredJwtException e) {
                e.printStackTrace();
                return null;
            } catch (UnsupportedJwtException e) {
                e.printStackTrace();
                return null;
            } catch (MalformedJwtException e) {
                e.printStackTrace();
                return null;
            } catch (SignatureException e) {
                e.printStackTrace();
                return null;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }
}