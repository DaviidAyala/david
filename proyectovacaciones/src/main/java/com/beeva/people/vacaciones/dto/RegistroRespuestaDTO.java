package com.beeva.people.vacaciones.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.beeva.people.vacaciones.empleado.pojo.RegistroVacaciones;

public class RegistroRespuestaDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long numeroEmpleado;
	private String nombre;
	private Date fechaIngreso;
	private int diasDisponiblesEmpleado;
	private int diasUtilizados;
	private int diasCorrespondientes;
	private List<RegistroVacaciones> vacaciones;
	
	public Long getNumeroEmpleado() {
		return numeroEmpleado;
	}
	public void setNumeroEmpleado(Long numeroEmpleado) {
		this.numeroEmpleado = numeroEmpleado;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public int getDiasDisponiblesEmpleado() {
		return diasDisponiblesEmpleado;
	}
	public void setDiasDisponiblesEmpleado(int diasDisponiblesEmpleado) {
		this.diasDisponiblesEmpleado = diasDisponiblesEmpleado;
	}
	public int getDiasUtilizados() {
		return diasUtilizados;
	}
	public void setDiasUtilizados(int diasUtilizados) {
		this.diasUtilizados = diasUtilizados;
	}
	public int getDiasCorrespondientes() {
		return diasCorrespondientes;
	}
	public void setDiasCorrespondientes(int diasCorrespondientes) {
		this.diasCorrespondientes = diasCorrespondientes;
	}
	public List<RegistroVacaciones> getVacaciones() {
		return vacaciones;
	}
	public List<RegistroVacaciones> setVacaciones(List<RegistroVacaciones> vacaciones) {
		return this.vacaciones = vacaciones;
	}
	
	


}
