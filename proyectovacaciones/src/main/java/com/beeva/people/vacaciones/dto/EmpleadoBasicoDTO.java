package com.beeva.people.vacaciones.dto;

import java.io.Serializable;

public class EmpleadoBasicoDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long numeroEmpleado;
	private String nombreCompleto;
	private int diasDisponiblesEmpleado;
	
	public Long getNumeroEmpleado() {
		return numeroEmpleado;
	}

	public void setNumeroEmpleado(Long numeroEmpleado) {
		this.numeroEmpleado = numeroEmpleado;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public int getDiasDisponiblesEmpleado() {
		return diasDisponiblesEmpleado;
	}

	public void setDiasDisponiblesEmpleado(int diasDisponiblesEmpleado) {
		this.diasDisponiblesEmpleado = diasDisponiblesEmpleado;
	}

}
