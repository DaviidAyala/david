package com.beeva.people.vacaciones.empleado.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.beeva.people.vacaciones.empleado.pojo.Empleado;

public interface EmpleadoRepository extends CrudRepository<Empleado, Long>, QueryByExampleExecutor<Empleado> {

	@Query("SELECT e.fechaIngreso FROM Empleado e WHERE e.id = :id")
	public Date getDateCreate(@Param("id") Long id);

	public List<Empleado> findByNombre(String nombre);
	
	public List<Empleado> findEmpeladoById(Long id);

	public List<Empleado> findByNombreAndApellidoPaternoAndApellidoMaterno(String nombre, String apellidoPat,
			String apellidoMat);
}
