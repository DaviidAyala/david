package com.beeva.people.vacaciones.utils;

import java.text.SimpleDateFormat;

public class ValidadorFecha {
	
	public static boolean fechaValida(String fecha) {
		try {
			SimpleDateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");
			formatoFecha.setLenient(false);
			formatoFecha.parse(fecha);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
