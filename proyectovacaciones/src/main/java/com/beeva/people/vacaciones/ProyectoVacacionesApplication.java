package com.beeva.people.vacaciones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoVacacionesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoVacacionesApplication.class, args);
	}
}
