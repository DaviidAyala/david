package com.beeva.people.vacaciones.utils;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.beeva.people.vacaciones.dto.EmpleadoBasicoDTO;
import com.beeva.people.vacaciones.dto.EmpleadoRespuestaDTO;
import com.beeva.people.vacaciones.dto.RegistroRespuestaDTO;
import com.beeva.people.vacaciones.empleado.pojo.Empleado;
import com.beeva.people.vacaciones.empleado.pojo.RegistroVacaciones;

public class Registro {	
	public RegistroRespuestaDTO respuestaDTO(Empleado empleado) {
		int diasTomados = 0, diasRestantes = 0, diasTrabajados = 0, diasDisponibles = 0;
		
		List<RegistroVacaciones> listaVacaciones = new ArrayList<>();
		RegistrarVacaciones dias = new RegistrarVacaciones();
		RegistroRespuestaDTO dto;
		Date fechaActual = new Date();

		dto = new RegistroRespuestaDTO();
		dto.setNombre(empleado.getNombre() + " " + empleado.getApellidoPaterno() + " " + empleado.getApellidoMaterno());
		dto.setNumeroEmpleado(empleado.getNumeroEmpleado());
		dto.setFechaIngreso(empleado.getFechaIngreso());
		listaVacaciones = dto.setVacaciones(empleado.getVacaciones());

		for (RegistroVacaciones contadorDias : listaVacaciones) {
			diasTomados += contadorDias.getDiasTomados();
		}

		diasTrabajados = dias.DiasTranscurridos(empleado.getFechaIngreso(), fechaActual);
		diasDisponibles = dias.DiasPermitidos(diasTrabajados);
		diasRestantes = diasDisponibles - diasTomados;

		dto.setDiasUtilizados(diasTomados);
		dto.setDiasCorrespondientes(diasDisponibles);
		dto.setDiasDisponiblesEmpleado(diasRestantes);

		return dto;
	}

	public int createRegistroVacaciones(List<RegistroVacaciones> listRegistroVacaciones, Empleado empleado,
			Date fechaInicial) {

		int diasTrabajados = 0, diasPermitidos = 0, diasTomados = 0, diasRestantes = 0;

		RegistrarVacaciones dias = new RegistrarVacaciones();
		Date fechaInicio = empleado.getFechaIngreso();
		for (RegistroVacaciones contadorDias : listRegistroVacaciones) {
			diasTomados += contadorDias.getDiasTomados();
		}

		diasTrabajados = dias.DiasTranscurridos(fechaInicio, fechaInicial);
		diasPermitidos = dias.DiasPermitidos(diasTrabajados);
		diasRestantes = diasPermitidos - diasTomados;

		return diasRestantes;
	}

	public EmpleadoRespuestaDTO messageEmpleado(ArrayList<Date> registroVacaciones, Empleado empleado) {

		EmpleadoRespuestaDTO dto = new EmpleadoRespuestaDTO();
		ArrayList<String> lista = new ArrayList<>();

		if(registroVacaciones == null) {
			dto.setFechas(null);
			dto.setNombre(empleado.getNombre() + " " + empleado.getApellidoPaterno() +" "+ empleado.getApellidoMaterno());
			dto.setMensaje("No cuenta con días disponobles");
		}else {
			for (Date d : registroVacaciones) {
				Format format = new SimpleDateFormat("dd-MM-yyyy");
				String s = format.format(d);
				lista.add(s);
			}
			dto.setFechas(lista);
			dto.setMensaje("Vacaciones aprobadas");
			dto.setNombre(empleado.getNombre() + " " + empleado.getApellidoPaterno() + " " + empleado.getApellidoMaterno());
		}
		return dto;
	}

	public String convertDateToString(Date date) {
		Format format = new SimpleDateFormat("dd-MM-yyyy");
		String s = format.format(date);
		return s;
	}

	public List<EmpleadoBasicoDTO> empeladoBasicoDTO(List<Empleado> lista) {
		int diasTomados = 0, diasRestantes = 0, diasTrabajados = 0, diasDisponibles = 0;

		List<EmpleadoBasicoDTO> registros = new ArrayList<>();
		List<RegistroVacaciones> listaVacaciones = new ArrayList<>();
		RegistrarVacaciones dias = new RegistrarVacaciones();
		EmpleadoBasicoDTO dto;
		Date fechaActual = new Date();

		for (Empleado empleado : lista) {
			dto = new EmpleadoBasicoDTO();
			dto.setNombreCompleto(
					empleado.getNombre() + " " + empleado.getApellidoPaterno() + " " + empleado.getApellidoMaterno());
			dto.setNumeroEmpleado(empleado.getNumeroEmpleado());
			listaVacaciones = empleado.getVacaciones();

			for (RegistroVacaciones contadorDias : listaVacaciones) {
				diasTomados += contadorDias.getDiasTomados();
			}
			diasTrabajados = dias.DiasTranscurridos(empleado.getFechaIngreso(), fechaActual);
			diasDisponibles = dias.DiasPermitidos(diasTrabajados);
			diasRestantes = diasDisponibles - diasTomados;
			dto.setDiasDisponiblesEmpleado(diasRestantes);
			registros.add(dto);
		}
		return registros;
	}
}
