package com.beeva.people.vacaciones.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RegistroVacacionesDTO implements Serializable{
private static final long serialVersionUID = 1L;
	//DTO Registro Vacaciones
	
	private Long idRegistro;
	private List<Date> fecha;
	private Long diasTomados;
	private Long idEmpleado;
	
	public Long getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(Long idRegistro) {
		this.idRegistro = idRegistro;
	}
	
	public Long getDiasTomados() {
		return diasTomados;
	}
	public void setDiasTomados(Long diasTomados) {
		this.diasTomados = diasTomados;
	}
	public Long getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(Long idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public List<Date> getFecha() {
		return fecha;
	}
	public void setFecha(List<Date> fecha) {
		this.fecha = fecha;
	}
	
	
	
}
