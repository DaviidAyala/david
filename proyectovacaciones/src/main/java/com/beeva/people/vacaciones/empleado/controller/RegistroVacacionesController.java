package com.beeva.people.vacaciones.empleado.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beeva.people.vacaciones.empleado.pojo.RegistroVacaciones;
import com.beeva.people.vacaciones.empleado.service.RegistroVacacionesService;

@Controller
@RequestMapping(path = "/vacaciones")
public class RegistroVacacionesController {

	@Autowired
	private RegistroVacacionesService regService;

	@GetMapping(path = "/registros")
	public @ResponseBody Iterable<RegistroVacaciones> getRegistros() {
		return regService.findAll();
	}

	@GetMapping(path = "/dias-tomados/{id}")
	public @ResponseBody Long getDiasTomados(@PathVariable Long id) {
		return regService.countByIdEmpleado(id);
	}

}
