package com.beeva.people.vacaciones.empleado.service;

import java.sql.Date;
import java.util.List;

import com.beeva.people.vacaciones.empleado.pojo.Empleado;

public interface EmpleadoService {

	public Empleado findById(Long id);

	public List<Empleado> findAll();

	public Empleado save(Empleado empleado);

	public void delete(Long id);

	public Date findDate(Long id);

	public List<Empleado> findByNombre(String nombre);
	
	public List<Empleado> findEmpeladoById(Long id);

	public List<Empleado> findByNombreAndApellidoPaternoAndApellidoMaterno(String nombre, String apellidoPat,
			String apellidoMat);

	public Iterable<Empleado> findEmpleados(Empleado empleado);

}
