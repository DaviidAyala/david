package com.beeva.people.vacaciones.empleado.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.beeva.people.vacaciones.dto.EmpleadoBasicoDTO;
import com.beeva.people.vacaciones.dto.EmpleadoRespuestaDTO;
import com.beeva.people.vacaciones.dto.RegistroRespuestaDTO;
import com.beeva.people.vacaciones.dto.VacacionesDTO;
import com.beeva.people.vacaciones.empleado.pojo.Empleado;
import com.beeva.people.vacaciones.empleado.pojo.RegistroVacaciones;
import com.beeva.people.vacaciones.empleado.service.EmpleadoService;
import com.beeva.people.vacaciones.empleado.service.RegistroVacacionesService;
import com.beeva.people.vacaciones.utils.RegistrarVacaciones;
import com.beeva.people.vacaciones.utils.Registro;

@RestController
@RequestMapping("/control-empleados")
public class EmpleadoRestController {

	@Autowired
	private EmpleadoService empleadoService;

	@Autowired
	private RegistroVacacionesService registroVacacionesService;

	@GetMapping("/consultar-empleados")
	public List<Empleado> getAllEmpleados() {
		return empleadoService.findAll();
	}

	@PreAuthorize("hasRole('admin')")
	@GetMapping("/consultar-empleado/{id}")
	public Empleado getEmpleado(@PathVariable Long id) {
		return empleadoService.findById(id);
	}

	@PreAuthorize("hasRole('admin')")
	@GetMapping("/empleados/nombre/{nombre}")
	public List<Empleado> getEmpleadoByNombre(@PathVariable String nombre) {
		return empleadoService.findByNombre(nombre);
	}

	@PreAuthorize("hasRole('admin')")
	@GetMapping("/dias-disponibles-empleado/{id}")
	public int diasDisponiblesEmpleado(@PathVariable Long id) {
		int diasTrabajados = 0, diasPermitidos = 0;
		Empleado empleado = new Empleado();
		Date date = new Date();

		RegistrarVacaciones dias = new RegistrarVacaciones();

		empleado = empleadoService.findById(id);
		Date fechaInicial = empleado.getFechaIngreso();
		diasTrabajados = dias.DiasTranscurridos(fechaInicial, date);
		diasPermitidos = dias.DiasPermitidos(diasTrabajados);

		return diasPermitidos;
	}

	@PreAuthorize("hasRole('admin')")
	@GetMapping("/informacion-empleado/{id}")
	public RegistroRespuestaDTO getRegistroByNombre(@PathVariable Long id) {
		Empleado datos = empleadoService.findById(id);
		Registro registro = new Registro();
		return registro.respuestaDTO(datos);
	}
	
	@PreAuthorize("hasRole('admin')")
	@DeleteMapping("/empleados/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteEmpleado(@PathVariable Long id) {
		empleadoService.delete(id);
	}

	@PreAuthorize("hasRole('admin')")
	@PostMapping("/registrar-vacaciones")
	@ResponseStatus(HttpStatus.CREATED)
	public EmpleadoRespuestaDTO createRegistroVacaciones(@RequestBody RegistroVacaciones registroVacaciones) {
		Registro registro = new Registro();
		Empleado empleado = new Empleado();
		EmpleadoRespuestaDTO dto = new EmpleadoRespuestaDTO();

		List<RegistroVacaciones> listRegistroVacaciones = new ArrayList<>();
		ArrayList<Date> listaDate = new ArrayList<>();

		listRegistroVacaciones = registroVacacionesService.findByIdEmpleado(registroVacaciones.getIdEmpleado());
		empleado = empleadoService.findById(registroVacaciones.getIdEmpleado());

		int diasRestantes = registro.createRegistroVacaciones(listRegistroVacaciones, empleado,
				registroVacaciones.getFecha());

		if (registroVacaciones.getDiasTomados() <= diasRestantes) {
			RegistroVacaciones vaciones = registroVacacionesService.save(registroVacaciones);
			listaDate.add(vaciones.getFecha());
			dto = registro.messageEmpleado(listaDate, empleado);
		}

		return dto;
	}

	@PreAuthorize("hasRole('admin')")
	@PostMapping("/registrar-fechas")
	@ResponseStatus(HttpStatus.CREATED)
	public EmpleadoRespuestaDTO arrayVacaciones(@RequestBody VacacionesDTO vacacionesDTO) {
   		RegistrarVacaciones registrarVacaciones= new RegistrarVacaciones(); 
		List<RegistroVacaciones> listaRegistroVacaciones= new ArrayList<>();
		ArrayList<Date> listaDate = new ArrayList<>();
		EmpleadoRespuestaDTO dto = new EmpleadoRespuestaDTO();
		Registro registro = new Registro();
		listaRegistroVacaciones=registrarVacaciones.RegistroArrayVacaciones(vacacionesDTO);
		int diasPermitidos=0,diasTomados=0;
		Empleado empleado= new Empleado();
		
		empleado=empleadoService.findById(vacacionesDTO.getIdEmpleado());
		
		for(RegistroVacaciones registroVacaciones : listaRegistroVacaciones) {	
			diasPermitidos=registrarVacaciones.DiasPermitidos(registrarVacaciones.DiasTranscurridos(empleado.getFechaIngreso(),  registroVacaciones.getFecha()));
			diasTomados=registroVacacionesService.findByIdEmpleado(empleado.getNumeroEmpleado()).size();	
			if(diasPermitidos > diasTomados ) {
				listaDate.add(registroVacaciones.getFecha());
				registroVacacionesService.save(registroVacaciones);
				dto = registro.messageEmpleado(listaDate, empleado);
			}else if(diasPermitidos == 0){
				dto = registro.messageEmpleado(null, empleado);
			}
		}
		return dto;
	}

	@PreAuthorize("hasRole('admin')")
	@PostMapping("/buscar-empleados")
	public List<Empleado> getEmpleadoByNombreAndApellidos(@RequestBody Empleado empleado) {
		return empleadoService.findByNombreAndApellidoPaternoAndApellidoMaterno(empleado.getNombre(),
				empleado.getApellidoPaterno(), empleado.getApellidoMaterno());
	}
	
	@PreAuthorize("hasRole('admin')")
	@PostMapping("/informacion-basica-empleado")
	public List<EmpleadoBasicoDTO> getInformacionEmpleadoById(@RequestBody Empleado empleado) {
		List<Empleado> datos = empleadoService.findEmpeladoById(empleado.getNumeroEmpleado());
		Registro registro = new Registro();
		return registro.empeladoBasicoDTO(datos);
	}

	@PreAuthorize("hasRole('admin')")
	@PostMapping("/crear-empleado")
	@ResponseStatus(HttpStatus.CREATED)
	public Empleado createEmpleado(@RequestBody Empleado empleado) {
		return empleadoService.save(empleado);
	}

	@PreAuthorize("hasRole('admin')")
	@PutMapping("/modificar-empleado/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Empleado updateEmpleado(@RequestBody Empleado empleado, @PathVariable Long id) {
		Empleado empleadoActual = empleadoService.findById(id);

		empleadoActual.setNombre(empleado.getNombre());
		empleadoActual.setApellidoMaterno(empleado.getApellidoMaterno());
		empleadoActual.setApellidoPaterno(empleado.getApellidoPaterno());

		return empleadoService.save(empleadoActual);
	}
	
	@GetMapping("/consultar-empleado-map")
	public Iterable<Empleado> getEmpleado(@RequestParam Map<String, String> map) {
		Empleado emp = new Empleado();
		if (map.containsKey("numeroEmpleado")) {
			emp.setNumeroEmpleado(Long.valueOf(map.get("numeroEmpleado")));
		}
		emp.setNombre(map.get("nombre"));
		emp.setApellidoPaterno(map.get("apellidoPaterno"));
		emp.setApellidoMaterno(map.get("apellidoMaterno"));
		return empleadoService.findEmpleados(emp);
	}

}
