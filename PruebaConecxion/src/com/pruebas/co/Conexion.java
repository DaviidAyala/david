package com.pruebas.co;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JOptionPane;

public class Conexion {
	static Connection conn = null;
	static String driver = "net.ucanaccess.jdbc.UcanaccessDriver";

	static String url = "jdbc:ucanaccess://C:\\Users\\Equipo\\Documents\\Aplicacion Biometrico\\ATT2000.MDB";

	public static Connection ejecutarConexion(){
	    try{
	        if(conn==null){
	            Class.forName(driver);
	            conn = DriverManager.getConnection(url);
	            JOptionPane.showMessageDialog(null, "Conectado");

	        }
	    } catch (Exception ex){
	        ex.printStackTrace();
	        conn = null;
	    }
	    return conn;
	}
	
	public static void main(String [] args){
	    Connection cn= Conexion.ejecutarConexion();
	}
}
